﻿using UnityEngine;

public class EndGamePanelControler : MonoBehaviour
{
    public string LevelName;

    public void ShowPanel()
    {
        gameObject.SetActive(true);
    }

    void OnMouseDown()
    {
        Application.LoadLevel(LevelName);
    }
}
