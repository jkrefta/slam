﻿using System;
using Random = UnityEngine.Random;

namespace Assets.Extensions
{
    public static class StringExtensions
    {
        public static string GetRandomLetter(this string stringObject)
        {
            int randomIndex = Random.Range(0, stringObject.Length - 1);
            return stringObject.Substring(randomIndex, 1);
        }

        public static int GetFirstLetterValue(this string stringObject)
        {
            return stringObject[0];
        }
    }
}
