﻿using System;
using UnityEngine;

public class SymbolInteractionControler : MonoBehaviour
{
    public SymbolControler symbolControler;
    public int SymbolValue{ get; private set; }
    private Animator animator;

    void Start()
    {
        animator = GetComponentInChildren<Animator>();
    }

    public void UpdateSymbolValue(int symbolValue)
    {
        SymbolValue = symbolValue;
    }

    public int GetDifferenceWith(int value)
    {
        return Math.Abs(value - SymbolValue);
    }

    public void OnMouseDown()
    {
        symbolControler.SymbolChoiceAction(SymbolValue);
        animator.SetBool("tap", true);
    }
}
