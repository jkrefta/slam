﻿using UnityEngine;

public class NextRoundPanelControler : MonoBehaviour
{
    public TimeLeftDisplay TimeLeftDisplay;
    public double turnOffTime = 3;
    private bool timerIsOn = false;
    private double timeLeftToTurnOff;

	public void ShowPanel()
	{
	    timerIsOn = true;
	    timeLeftToTurnOff = turnOffTime;
        gameObject.SetActive(true);
    }

    void OnMouseDown()
    {
        ContinueGame();
    }

    void LateUpdate()
    {
        if (timerIsOn && timeLeftToTurnOff > 0)
        {
            timeLeftToTurnOff -= Time.deltaTime;
            if (timeLeftToTurnOff <= 0)
            {
                ContinueGame();
            }
        }
    }

    private void ContinueGame()
    {
        gameObject.SetActive(false);
        TimeLeftDisplay.StartTimeCount();
    }
}
