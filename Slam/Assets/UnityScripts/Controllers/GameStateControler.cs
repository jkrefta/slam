﻿using System.Collections.Generic;
using UnityEngine;

public class GameStateControler : MonoBehaviour
{
    public SymbolControler SymbolControler;
    public EndGamePanelControler EndGamePanel;
    public NextRoundPanelControler NextRoundPanel;
    public TimeLeftDisplay TimeLeftDisplay;
    public AudioClip positiveBep;
    public AudioClip negativeBep;
    public int Round = 1;
    public List<int> NextRoundScore;
    public List<double> RoundTime; 
    public int Lives = 3;
    public int Score = 0;
    public AudioSource AudioSource;

    public void ScoreUp()
    {
        Score += 10;
        if (isNextRound) AdvanceToNextRound();
        AudioSource.PlayOneShot(positiveBep);
        TimeLeftDisplay.SetTimeTo(RoundTime[Round]);
    }

    private void AdvanceToNextRound()
    {
        TimeLeftDisplay.StopTimeCount();
        NextRoundPanel.ShowPanel();
        Lives = 3;
        Round++;
    }

    private bool isNextRound
    {
        get { return Score >= NextRoundScore[Round]; }
    }

    public void LoseLife()
    {
        Lives -= 1;
        AudioSource.PlayOneShot(negativeBep);
        if (Lives <= 0)
        {
            EndGame();
            return;
        }
        TimeLeftDisplay.SetTimeTo(RoundTime[Round]);
    }

    public void TimeHasRunOut()
    {
        LoseLife();
    }

    void EndGame()
    {
        SymbolControler.TurnOff();
        EndGamePanel.ShowPanel();
        TimeLeftDisplay.isStopped = true;
    }
}
